package vaadin.recipes;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

/**
 * The main view contains a button and a click listener.
 */
@Route
@HtmlImport("frontend://styles/unround-combo-box-styles.html")
public class MainView extends VerticalLayout {

    public MainView() {
        //This is a regular combobox:
        add(new ComboBox<String>("Rounded corners"));

        //This one we want styled
        ComboBox<String> noRoundCornersComboBox = new ComboBox<>("Not rounded corners");
        noRoundCornersComboBox.addClassName("unround");
        add(noRoundCornersComboBox);
    }
}
